package com.example.demo;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.model.ReplyMessage;
import com.linecorp.bot.model.event.MessageEvent;
import com.linecorp.bot.model.event.message.TextMessageContent;
import com.linecorp.bot.model.message.TextMessage;
import com.linecorp.bot.spring.boot.annotation.EventMapping;
import com.linecorp.bot.spring.boot.annotation.LineMessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication (exclude={DataSourceAutoConfiguration.class})
@LineMessageHandler
public class BulletinTodoApplication extends SpringBootServletInitializer {

    @Autowired
    private LineMessagingClient lineMessagingClient;

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(BulletinTodoApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(BulletinTodoApplication.class, args);
    }

    @EventMapping
    public void handleTextEvent(MessageEvent<TextMessageContent> messageEvent){
        String pesan = messageEvent.getMessage().getText().toLowerCase();

        if(pesan.contains("hello")){
            String replyToken = messageEvent.getReplyToken();
            TextMessage jawab = new TextMessage("Hello Pengguna");

            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawab));
        }

        else if(pesan.contains("help")){
            String replyToken = messageEvent.getReplyToken();
            TextMessage jawab = new TextMessage("Daftar command yang tersedia: \\n1. list \\n2. add 'todo'\\n3. edit 'todo1' 'todo2'\\n4. delete 'todo'");

            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawab));
        }

        else if(pesan.contains("list")){
            String replyToken = messageEvent.getReplyToken();
            TextMessage jawab = new TextMessage("Saat ini fitur list saat ini sedang dalam pengembangan");

            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawab));
        }

        else if(pesan.contains("add")){
            String replyToken = messageEvent.getReplyToken();
            TextMessage jawab = new TextMessage("Saat ini fitur add saat ini sedang dalam pengembangan");

            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawab));
        }

        else if(pesan.contains("edit")){
            String replyToken = messageEvent.getReplyToken();
            TextMessage jawab = new TextMessage("Saat ini fitur edit saat ini sedang dalam pengembangan");

            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawab));
        }

        else if(pesan.contains("delete")){
            String replyToken = messageEvent.getReplyToken();
            TextMessage jawab = new TextMessage("Saat ini fitur delete saat ini sedang dalam pengembangan");

            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawab));
        }

        else {
            String replyToken = messageEvent.getReplyToken();
            TextMessage jawab = new TextMessage("Halo! ketik 'help' untuk melihat daftar command yang ada");

            lineMessagingClient
                    .replyMessage(new ReplyMessage(replyToken, jawab));
        }
    }

}
